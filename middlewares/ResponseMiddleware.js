module.exports = function () {
  return function (req, res, next) {
    // res.serverError = function () {};

    // res.notFound = function () {};

    // res.badRequest = function (error) {
    //   let message = "Bad Request";
    //   let code = null;
    //   let list = [];

    //   if (error.message) {
    //     message = error.message;
    //   }

    //   if (error.code) {
    //     code = error.code;
    //   }

    //   if (error.list) {
    //     list = error.list;
    //   }
    // };

    res.result = function (data, meta) {
      let response = {
        data,
      };
      if (meta) {
        response.meta = metadata;
      }
      res.json(response);
    };

    next();
  };
};

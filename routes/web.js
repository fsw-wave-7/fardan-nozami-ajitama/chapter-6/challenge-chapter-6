const { Router } = require("express");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const AuthMiddleware = require("../middlewares/AuthMiddleware");

const AuthController = require("../controllers/web/AuthController");
const HomeController = require("../controllers/web/HomeController");

const web = Router();

web.use(bodyParser.json());
web.use(bodyParser.urlencoded({ extended: true }));
web.use(cookieParser());

const authController = new AuthController();
const homeController = new HomeController();

web.get("/login", authController.login);
web.post("/login", authController.doLogin);
web.get("/logout", authController.logout);

// web.use(AuthMiddleware());

web.get("/", homeController.dashboard);
web.get("/users", homeController.users);
web.post("/save-user", homeController.saveUser);
web.get("/add-user", homeController.addUser);
web.get("/edit/:id", homeController.editUser);
web.post("/edit/:id", homeController.updateUser);
web.get("/delete/:id", homeController.deleteUser);
web.get("/skor/:id", homeController.skor);
web.post("/skor/:id", homeController.saveSkor);

module.exports = web;

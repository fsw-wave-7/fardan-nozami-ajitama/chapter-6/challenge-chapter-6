const { User, Biodata, History } = require("../../models");
const bcrypt = require("bcrypt");

class HomeController {
  dashboard = (req, res) => {
    User.findAll({
      include: [
        {
          model: Biodata,
          as: "user_biodata",
        },
        {
          model: History,
          as: "user_history",
        },
      ],
      order: [["createdAt", "DESC"]],
    }).then((users) => {
      console.log(JSON.stringify(users[0].user_history[0].skor));

      res.render("dashboard", {
        content: "./pages/dashboard",
        title: "Users",
        users,
      });
    });
  };

  users = (req, res) => {
    User.findAll({
      include: [
        {
          model: Biodata,
          as: "user_biodata",
        },
      ],
    }).then((users) => {
      res.render("dashboard", {
        content: "./pages/users",
        title: "Users",
        users,
      });
    });
  };

  addUser = (req, res) => {
    res.render("dashboard", {
      content: "./pages/add-user",
      title: "Add user",
    });
  };

  saveUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10);

    User.create(
      {
        username: req.body.username,
        password: await bcrypt.hash(req.body.password, salt),
        user_biodata: {
          name: req.body.name,
          address: req.body.address,
        },
      },
      {
        include: [
          {
            model: Biodata,
            as: "user_biodata",
          },
        ],
      }
    )
      .then(() => {
        res.redirect("/users");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  editUser = (req, res) => {
    const id = req.params.id;

    User.findOne({
      where: { id },
      include: {
        model: Biodata,
        as: "user_biodata",
      },
    }).then((user) => {
      res.render("dashboard", {
        content: "./pages/edit-user",
        title: "User",
        user,
      });
    });
  };

  updateUser = async (req, res) => {
    const salt = await bcrypt.genSalt(10);
    User.update(
      {
        username: req.body.username,
        password: await bcrypt.hash(req.body.password, salt),
      },
      {
        where: { id: req.params.id },
      }
    );
    Biodata.update(
      {
        name: req.body.name,
        address: req.body.address,
      },
      {
        where: { userId: req.params.id },
      }
    )
      .then(() => {
        res.redirect("/users");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  skor = (req, res) => {
    const id = req.params.id;

    User.findOne({
      where: { id },
      include: {
        model: History,
        as: "user_history",
      },
    }).then((user) => {
      res.render("dashboard", {
        content: "./pages/skor",
        title: "skor",
        user,
      });
    });
  };

  saveSkor = (req, res) => {
    History.create({
      skor: req.body.skor,
      userId: req.params.id,
    })
      .then(() => {
        res.redirect("/users");
      })
      .catch((err) => {
        console.log(err);
      });
  };

  deleteUser = (req, res) => {
    User.destroy({
      where: { id: req.params.id },
    });
    Biodata.destroy({
      where: { userId: req.params.id },
    }).then(() => {
      res.redirect("/users");
    });
  };
}

module.exports = HomeController;
